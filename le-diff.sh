#!/bin/sh

expr=`pwd`
proj=`grep projdir ./.le/.config | sed "s/projdir //"`
ignored=`grep ignore "$expr/.le/.config" | sed "s/ignore //" | tr "\n" "|" | sed "s/|$//"`

cd "$proj"; find . -maxdepth 1 -type f -not -name ".*" | sed "s/.\///" > "$expr/porov"
cd "$expr"; find . -maxdepth 1 -type f -not -name ".*" | sed "s/.\///" | grep -E -v "(`tr "\n" "|" < ./porov | sed "s/|$//"`|porov)" >> "$expr/porov"

if [ ! $# -eq 0 ]; then
	echo -n "" > "$expr/seznam"
	for var in "$@"
	do
		echo "$var" >> "$expr/seznam"
	done
	grep -E -o "(`tr "\n" "|" < "$expr/seznam"`|porov)" porov > porov_sez
	mv porov_sez porov
	rm "$expr/seznam"
fi

if [ ! -z "$ignored" ]; then
	grep -E -v "(`echo $ignored`)" porov > porov_sez
	mv porov_sez porov
fi

while read soubor
do
	diff -u "$proj/$soubor" "$expr/$soubor" 2>/dev/null
	result=$?
	if [ $result -eq 2 ]; then
		if [ -e "$proj/$soubor" ]; then
			echo "C: $soubor"
		else
			echo "D: $soubor"
		fi
	elif [ $result -eq 0 ]; then
		echo ".: $soubor"
	fi
done < "$expr/porov"

rm "$expr/porov"
exit 0
