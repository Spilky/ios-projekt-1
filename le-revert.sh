#!/bin/sh

expr=`pwd`
cd ".le"
ignored=`grep ignore "$expr/.le/.config" | sed "s/ignore //" | tr "\n" "|" | sed "s/|$//"`

if [ $# -eq 0 ]; then
	if [ -z "$ignored" ]; then
		find . -maxdepth 1 -type f -not -name ".*" | sed "s/.\///" > "$expr/copy"
	else
		find . -maxdepth 1 -type f -not -name ".*" | sed "s/.\///" | grep -E -v "(`echo "$ignored"`)" > "$expr/copy"
	fi
else
	echo -n "" > "$expr/seznam"
	for var in "$@"
	do
		echo "$var" >> "$expr/seznam"
	done
	if [ -z "$ignored" ]; then
		find . -maxdepth 1 -type f -not -name ".*" | sed "s/.\///" | grep -E -o "(`tr "\n" "|" < "$expr/seznam"`)" > "$expr/copy"
	else
		find . -maxdepth 1 -type f -not -name ".*" | sed "s/.\///" | grep -E -v "(`echo "$ignored"`)" | grep -E -o "(`tr "\n" "|" < "$expr/seznam"`)" > "$expr/copy"
	fi
	rm "$expr/seznam"
fi
while read line
do
	cp "`echo $line`" "$expr/" >/dev/null 2>/dev/null
done < "$expr/copy"
rm "$expr/copy"
exit 0
