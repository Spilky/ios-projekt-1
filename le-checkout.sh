#!/bin/sh

expr=`pwd`
cd "$expr"

if [ -d ".le" ]; then
	rm ./.le/* 2>/dev/null
else
	mkdir .le
fi

if [ ! -d "$1" ]; then
	echo "Složka neexistuje. Skript ukončen.";
	exit 1
fi

if [ -e "./.le/.config" ]; then
	grep ignore ./.le/.config > ./.le/.config_doc
	mv "./.le/.config_doc" "./.le/.config"
	echo "projdir $1" >> ./.le/.config
else
	echo "projdir $1" > ./.le/.config
fi

cd "$1"
ignored=`grep ignore "$expr/.le/.config" | sed "s/ignore //" | tr "\n" "|" | sed "s/|$//"`
find . -maxdepth 1 -type f -not -name ".*" | sed "s/.\///" | grep -E -v "(`echo $ignored`)" > "$expr/copy"

if [ $? -eq 0 ]; then
	while read line
	do
		cp "`echo $line`" "$expr/.le/" >/dev/null 2>/dev/null
		cp "`echo $line`" "$expr/" >/dev/null 2>/dev/null
	done < "$expr/copy"
else
	cp ./* "$expr/.le/" >/dev/null 2>/dev/null
	cp ./* "$expr/" >/dev/null 2>/dev/null
fi

rm "$expr/copy"
exit 0
