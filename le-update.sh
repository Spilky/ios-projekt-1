#!/bin/sh

expr=`pwd`
proj=`grep projdir "./.le/.config" | sed "s/projdir //"`
ref="$expr/.le"
ignored=`grep ignore "$expr/.le/.config" | sed "s/ignore //" | tr "\n" "|" | sed "s/|$//"`

cd "$proj"; find . -maxdepth 1 -type f -not -name ".*" | sed "s/.\///" > "$expr/porov"
cd "$ref"; find . -maxdepth 1 -type f -not -name ".*" | sed "s/.\///" | grep -E -v "(`tr "\n" "|" < "$expr/porov" | sed "s/|$//"`|porov)" >> "$expr/porov"
cd "$expr"; find . -maxdepth 1 -type f -not -name ".*" | sed "s/.\///" | grep -E -v "(`tr "\n" "|" < "$expr/porov" | sed "s/|$//"`|porov)" >> "$expr/porov"

if [ ! $# -eq 0 ]; then
	echo -n "" > "$expr/seznam"
	for var in "$@"
	do
		echo "$var" >> "$expr/seznam"
	done
	grep -E -o "(`tr "\n" "|" < "$expr/seznam"`|porov)" porov > porov_sez
	mv porov_sez porov
	rm "$expr/seznam"
fi

if [ ! -z "$ignored" ]; then
	grep -E -v "(`grep ignore "$expr/.le/.config" | sed "s/ignore //" | tr "\n" "|" | sed "s/|$//"`)" porov > porov_sez
	mv porov_sez porov
fi

while read soubor
do
	diff "$ref/$soubor" "$proj/$soubor" 2>/dev/null >/dev/null; stat=$?
	if [ $stat -eq 0 ]; then
		diff "$ref/$soubor" "$expr/$soubor" 2>/dev/null >/dev/null; stat=$?
		if [ $stat -eq 0 ]; then
			sit=1
		elif [ $stat -eq 1 ]; then
			sit=2
		fi
	elif [ $stat -eq 1 ]; then
		diff "$ref/$soubor" "$expr/$soubor" 2>/dev/null >/dev/null; stat=$?
		if [ $stat -eq 0 ]; then
			sit=4
		elif [ $stat -eq 1 ]; then
			diff "$proj/$soubor" "$expr/$soubor" 2>/dev/null >/dev/null; stat=$?
			if [ $stat -eq 0 ]; then
				sit=3
			elif [ $stat -eq 1 ]; then
				sit=5
			fi
		fi
	fi
	
	if [ -e "$proj/$soubor" ]; then
		if [ ! -e "$expr/$soubor" ]; then
			sit=6
		fi
	else
		if [ -e "$ref/$soubor" ]; then
			sit=7
		fi
	fi
	
	case $sit in
		1)
			echo ".: $soubor"
			;;
		2)
			echo "M: $soubor"
			;;
		3)
			echo "UM: $soubor"
			cp "$proj/$soubor" "$ref/$soubor" >/dev/null 2>/dev/null
			;;
		4)
			echo "U: $soubor"
			cp "$proj/$soubor" "$expr/$soubor" >/dev/null 2>/dev/null
			cp "$proj/$soubor" "$ref/$soubor" >/dev/null 2>/dev/null
			;;
		5)
			diff -u  "$ref/$soubor" "$proj/$soubor" >$expr/diff.patch
			patch "$expr/$soubor" "$expr/diff.patch" >/dev/null 2>/dev/null
			if [ $? -eq 0 ]; then
				echo "M+: $soubor"
				cp "$proj/$soubor" "$ref/$soubor" >/dev/null 2>/dev/null
			else
				echo "M!: $soubor conflict!"
			fi
			rm "$expr/diff.patch" >/dev/null 2>/dev/null
			rm "$expr/$soubor.rej" >/dev/null 2>/dev/null
			rm "$expr/$soubor.orig" >/dev/null 2>/dev/null
			;;
		6)
			echo "C: $soubor"
			cp "$proj/$soubor" "$expr/$soubor" >/dev/null 2>/dev/null
			cp "$proj/$soubor" "$ref/$soubor" >/dev/null 2>/dev/null
			;;
		7)
			echo "D: $soubor"
			rm "$expr/$soubor"
			rm "$ref/$soubor"
			;;
		*)
	esac
	sit=0
done <"$expr/porov"

rm "$expr/porov"
exit 0
